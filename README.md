DOWNLOAD THE RANDOMIZER HERE: https://bitbucket.org/DeferredGalaxy3/idea-randomizer/downloads/DG's%20Randomizer.zip

Version 0.2.1
-Revamped method of category referencing. Now you can add as many categories as you want and reference them as many times as you want. You can even reference multiple categories in one reference for the randomize to group them together for that scenario. You can create more dynamic ideas with this new system.
-Added show/hide filepath for those who screenshot their results and prefer to keep their computer names private.

Version 0.1.2
-Added warning messages.

Versoin 0.1.1
-Initial release.