﻿namespace WindowsFormsApplication2 {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose ();
            }
            base.Dispose (disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent () {
            this.results = new System.Windows.Forms.Label();
            this.openFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.fileLoad = new System.Windows.Forms.Label();
            this.textVisibility = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // results
            // 
            this.results.AutoSize = true;
            this.results.Location = new System.Drawing.Point(9, 86);
            this.results.Name = "results";
            this.results.Size = new System.Drawing.Size(0, 13);
            this.results.TabIndex = 0;
            this.results.Click += new System.EventHandler(this.results_Click);
            // 
            // openFile
            // 
            this.openFile.Location = new System.Drawing.Point(12, 60);
            this.openFile.Name = "openFile";
            this.openFile.Size = new System.Drawing.Size(75, 23);
            this.openFile.TabIndex = 1;
            this.openFile.Text = "Open File";
            this.openFile.UseVisualStyleBackColor = true;
            this.openFile.Click += new System.EventHandler(this.openFile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(203, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Randomize";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // fileLoad
            // 
            this.fileLoad.AutoSize = true;
            this.fileLoad.Location = new System.Drawing.Point(9, 31);
            this.fileLoad.Name = "fileLoad";
            this.fileLoad.Size = new System.Drawing.Size(109, 13);
            this.fileLoad.TabIndex = 3;
            this.fileLoad.Text = "Please Load Text File";
            this.fileLoad.Click += new System.EventHandler(this.fileLoad_Click);
            // 
            // textVisibility
            // 
            this.textVisibility.Location = new System.Drawing.Point(12, 5);
            this.textVisibility.Name = "textVisibility";
            this.textVisibility.Size = new System.Drawing.Size(75, 23);
            this.textVisibility.TabIndex = 4;
            this.textVisibility.Text = "Hide Path";
            this.textVisibility.UseVisualStyleBackColor = true;
            this.textVisibility.Visible = false;
            this.textVisibility.Click += new System.EventHandler(this.textVisibility_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 108);
            this.Controls.Add(this.textVisibility);
            this.Controls.Add(this.fileLoad);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.openFile);
            this.Controls.Add(this.results);
            this.Name = "Form1";
            this.Text = "DG\'s Draw Idea Randomizer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label results;
        private System.Windows.Forms.Button openFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label fileLoad;
        private System.Windows.Forms.Button textVisibility;
    }
}

