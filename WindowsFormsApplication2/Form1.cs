﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2 {
    public partial class Form1 : Form {

        Random random = new Random ();
        bool showText = true;

        public List<string> getCategory (string categoryName) {
            List<string> testGetCategory = new List<string>();
            bool collectNames = false;
            for (int i = 0; i < textFile.Length; i++) {
                if (collectNames) {
                    if (textFile[i].Length > 1) {
                        if (textFile [i].Substring (0, 2) == "* ") {
                            testGetCategory.Add (textFile [i].Substring (2));
                        }
                    } else {
                        collectNames = false;
                    }
                }
                if (textFile [i] == "-- " + categoryName + " --") {
                    collectNames = true;
                }
            }
            return testGetCategory;
        }
        public List<string> Actions()
        {
            return getCategory("Actions");
        }
        public List<string> Category(string name) {
            return getCategory (name);
        }
        public string [] textFile;

        public Form1 () {
            InitializeComponent ();
        }

        private void results_Click (object sender, EventArgs e) {
        }

        private void openFile_Click (object sender, EventArgs e) {
            if (openFileDialog1.ShowDialog () == DialogResult.OK) {
                button1.Enabled = true;
                fileLoad.Text = openFileDialog1.FileName;
                textFile = System.IO.File.ReadAllLines (openFileDialog1.FileName);
                textVisibility.Visible = true;
            }
        }

        private void openFileDialog1_FileOk (object sender, CancelEventArgs e) {

        }

        public bool IsOdd(int number) {
            while (number > 0) {
                number -= 2;
            }
            if (number < 0) {
                return true;
            } else {
                return false;
            }
        }
        public string FullPhrase (string actionPhrase) {
                string [] halves = actionPhrase.Split ('(', ')');
                string fullPhrase = "";
                for (int i = 0; i < halves.Length; i++) {
                    if (IsOdd (i)) {
                        string [] categories = halves [i].Split ('|');
                        List<string> availableStrings = new List<string> ();
                        for (int x = 0; x < categories.Length; x++) {
                            for (int y = 0; y < Category (categories [x]).Count; y++) {
                                availableStrings.Add (Category (categories [x]) [y]);
                            }
                        }
                        halves [i] = categories [random.Next (categories.Length)];
                        halves [i] = availableStrings [random.Next (availableStrings.Count)];
                    }
                    fullPhrase = fullPhrase + halves [i];
                }
                return fullPhrase + ".";
        }

        private bool LineIsInFile (string name) {
            bool found = false;
            for (int i = 0; i < textFile.Length; i++) {
                if (name == textFile [i]) {
                    found = true;
                }
            }
            return found;
        }

        private int Line (string name) {
            int line = 0;
            for (int i = 0; i < textFile.Length; i++) {
                if (name == textFile [i]) {
                    line = i;
                }
            }
            return line;
        }
        private void button1_Click (object sender, EventArgs e) {
            try {
                results.Text = FullPhrase (Actions () [random.Next (Actions ().Count)]);
            } catch {
                string message = "Something went wrong! Fix the errors and reload the file! This is important for making the randomizer fuction properly! \n";
                if (LineIsInFile ("-- Actions --")) {
                    if (Line ("-- Actions --") + 1 == textFile.Length) {
                        message = message + "\n- You do not have any actions under your Actions category. List one with a bullet point.";
                    } else if (textFile [Line ("-- Actions --") + 1].Length == 0) {
                        if (Actions ().Count == 0) {
                            message = message + "\n- You do not have any actions under your Actions category. List one with a bullet point.";
                        }
                    }
                } else {
                    message = message + "\n- You are missing the -- Actions -- category.";
                }
                MessageBox.Show (message);
                return;
            }
        }

        private void Form1_Load (object sender, EventArgs e) {
        }

        private void fileLoad_Click (object sender, EventArgs e) {

        }

        private void textVisibility_Click (object sender, EventArgs e) {
            if (showText) {
                showText = false;
                textVisibility.Text = "Show Path";
            } else {
                showText = true;
                textVisibility.Text = "Hide Path";
            }
            fileLoad.Visible = showText;
        }
    }
}
